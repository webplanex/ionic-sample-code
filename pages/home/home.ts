import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { NavParams, LoadingController } from 'ionic-angular';
import { ListProvider } from '../../providers/list/list';
import { DetailProvider } from '../../providers/detail/detail';
import { DetailPage } from '../../pages/detail/detail';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  worker: any;
  workers: any;

  constructor(public platform: Platform,public navCtrl: NavController, public translate: TranslateService, public loadingController: LoadingController, public navParams: NavParams, public listProvider: ListProvider, public detailProvider: DetailProvider) {
    if(this.navParams.get('workers')) {
      this.workers = this.navParams.get('workers');
    } else {
      this.getWorkers();
    }
  }

  // Get all workers
  getWorkers() {
    let loading = this.loadingController.create();
    loading.present();
    this.listProvider.getWorkers()
    .then(data => {
      this.workers = data;
      loading.dismiss();
      console.log(this.workers);
    });
  }

  // Get single worker
  getWorker(id) {
    this.detailProvider.getWorker(id)
    .then(data => {
      this.worker = data;
      this.navCtrl.push(DetailPage, {
        worker: this.worker
      });
    });
  }

}
