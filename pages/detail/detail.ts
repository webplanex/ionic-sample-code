import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { DetailProvider } from '../../providers/detail/detail';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';

/**
 * Generated class for the DetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detail',
  templateUrl: 'detail.html',
})
export class DetailPage {

  private workerApply : FormGroup;

  worker: any;
  workerIds: any;

  apply = {
    id_worker: '',
    name: '',
    email: '',
    mobile_number: '',
    note: '',
    is_visa_ready: '',
    language: 'ltr'
  };

  constructor( public platform: Platform, private formBuilder: FormBuilder, public navCtrl: NavController, public navParams: NavParams, public detailProvider: DetailProvider, public translate: TranslateService) {

    this.getWorkerID();

    this.worker = this.navParams.get('worker');

    this.workerApply = this.formBuilder.group({
      id_worker:['', Validators.required],
      name: ['', Validators.required],
      email: [''],
      mobile_number: ['', Validators.compose([Validators.minLength(10), Validators.pattern('[0-9]*'), Validators.required])],
      note: ['', Validators.required],
      is_visa_ready: ['', Validators.required],
      language: ['']
    });

    this.apply.id_worker = this.worker.id;

    this.apply.language = 'ltr';

    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      if (event.lang == 'ar') {
        this.apply.language = 'rtl';
      } else {
        this.apply.language = 'ltr';
      }
    });

  }

  // Get single worker data from detailProvider
  getWorker(id) {
    this.detailProvider.getWorker(id)
    .then(data => {
      this.worker = data;
      console.log(this.worker);
    });
  }

  // Get all workers IDs from detailProvider
  getWorkerID() {
    this.detailProvider.getWorkerID()
    .then(data => {
      this.workerIds = data;
      console.log(this.apply.id_worker);
    });
  }

  // Submit apply worker form and send form data to detailProvider
  applyWorker() {
    this.detailProvider.applyWorker(this.apply).then((result) => {
      console.log(result);
      this.workerApply.reset();
    }, (err) => {
      console.log(err);
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailPage');
  }

}
