import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the ListProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ListProvider {

  // API URL to get data from server
  apiUrl = 'http://example.com';

  constructor(public http: HttpClient) {
    console.log('Hello ListProvider Provider');
  }

  // Method for get workers by API request
  getWorkers() {
    return new Promise(resolve => {
      this.http.get(this.apiUrl+'/lists').subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }


}
