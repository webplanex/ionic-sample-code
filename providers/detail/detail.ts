import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LoadingController, ToastController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

/*
  Generated class for the DetailProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DetailProvider {

  constructor(public http: HttpClient, public toastCtrl: ToastController, public loadingController: LoadingController, public translate: TranslateService) {
    console.log('Hello DetailProvider Provider');
  }

  // API URL to get data from server
  apiUrl = 'http://example.com';

  msg: any;

  // Get single worker details by ID
  getWorker(id) {

    let loading = this.loadingController.create({
      spinner: 'dots'
    });
    loading.present();

    return new Promise(resolve => {
      this.http.get(this.apiUrl+'/worker/detail/'+id).subscribe(data => {
        loading.dismiss();
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

  // Get all worker IDs
  getWorkerID() {
    return new Promise(resolve => {
      this.http.get(this.apiUrl+'/worker/ids').subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

  // API request for apply worker
  applyWorker(data) {
    let please_wait: any;
    this.translate.get(['PLEASE_WAIT']).subscribe(translations => {
      please_wait = translations.PLEASE_WAIT
    });
    let loading = this.loadingController.create({
      content: please_wait
    });

    loading.present();

    return new Promise((resolve, reject) => {
      this.http.post(this.apiUrl+'/worker/order/create', JSON.stringify(data))
        .subscribe(res => {
          resolve(res);
          loading.dismiss();

          this.msg = res;

          if(this.msg.success) {
            this.msg = this.msg.success;
          } else {
            this.msg = this.msg.error;
          }
          let toast = this.toastCtrl.create({
            message: this.msg,
            position: 'bottom',
            showCloseButton: true,
            closeButtonText: 'Ok'
          });
          toast.present();
        }, (err) => {
          let toast = this.toastCtrl.create({
            message: 'Something went wrong! Please try again.',
            position: 'bottom',
            showCloseButton: true,
            closeButtonText: 'Ok'
          });
          toast.present();
          reject(err);
        });
    });
  }

}
